#include "heap.h"
#include <stdio.h>
void init(HEAP *heap)
{
  heap -> N = 0;
}

int insert(HEAP *heap , elem x)
{
  int posParent,posCurrent;

  //An den uparxei xwros
  if(heap -> N == MAX_SIZE)
  return FALSE;

  //insert the new node
  heap -> data[ heap -> N ] = x;
  heap -> N++;

  //Swap with parent
  posCurrent =heap -> N-1;
  while (posCurrent > 0) {
    posParent = (posCurrent - 1) / 2;
    if( heap -> data[posCurrent] > heap -> data[posParent])
    {
      swap(&heap -> data[posParent],&heap -> data[posCurrent]);
      posCurrent=posParent;
    }else
      break;
  }
  return TRUE;
}

int delete(HEAP *heap , elem *x) // Diagrafi tis rizas
{
  int posCurrent , posLeft ,posRight , pos;
  if( heap -> N == 0)
    return FALSE;

    *x = heap -> data[0];

    heap -> data[0] = heap -> data[heap -> N-1]; // N represents the multitude of the array
    heap -> N --; //Root removed here

    //Algorith to fix the binary heapTree
    posCurrent = 0;
    while (posCurrent < heap -> N)
    {
        posLeft = 2 * posCurrent + 1;
        posRight = 2 * posCurrent + 2;

        if  (posLeft >= heap -> N)
          posLeft = -1;
        if  (posRight >= heap -> N)
          posRight = -1;

        if ( posLeft == -1 && posRight == -1)
        {
          break;
        }else if (posLeft != -1 && posRight == -1) //exei mono aristero
        {
          if( heap -> data[posCurrent] < heap -> data[posLeft])
          {
            swap(&heap->data[posCurrent],&heap->data[posLeft]);
            posCurrent = posLeft;
          }else{
            break;
          }
        }else if ( posLeft != -1 && posRight != -1) {
          //Check which of the two children have the greatest value
          if( heap -> data[posLeft] < heap -> data[posRight] )
          {
            pos = posRight;
          }else{
            pos = posLeft;
          }

          if(heap -> data[posCurrent] < heap -> data[pos])
          {
            swap(&heap -> data[posCurrent],&heap -> data[pos]);
            posCurrent = pos;
          }else{
            break;
          }
        }
    }
    return TRUE;
}

void HeapSort(HEAP *heap)
{
  elem x;
  while( heap -> N > 0)
  {
    delete(heap,&x);
    printf("%d\n", x);
  }
}

void swap(elem *x,elem *y)
{
  elem tmp = *x;
  *x = *y;
  *y = tmp;
}
