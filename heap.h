#ifndef  _HEAP_H_
#define  _HEAP_H_

#define TRUE 1
#define FALSE 0
#define MAX_SIZE 31
typedef int elem;

struct heapTree{
  elem data[MAX_SIZE];
  int N; //counter for the elements
};
typedef struct heapTree HEAP;

void init(HEAP *heap);
int insert(HEAP *heap , elem x);
void swap(elem *x,elem *y);
int delete(HEAP *heap , elem *x);
void HeapSort(HEAP *heap);
#endif
