#include <stdio.h>
#include <stdlib.h>
#include "heap.h"

int main(int argc, char const *argv[]) {
  HEAP heap;
  init(&heap);
  insert(&heap,5);
  insert(&heap,8);
  insert(&heap,14);
  insert(&heap,12);
  insert(&heap,7);
  insert(&heap,16);
  insert(&heap,13);
  insert(&heap,15);
  HeapSort(&heap);
  return 0;
}
